<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('users', 'App\Http\Controllers\Api\UsersController@index')->name('users');
Route::post('users/create', 'App\Http\Controllers\Api\UsersController@store')->name('users.create');
Route::post('users/edit', 'App\Http\Controllers\Api\UsersController@edit')->name('users.edit');
Route::post('users/show', 'App\Http\Controllers\Api\UsersController@show')->name('users.show');
Route::post('users/update/{id}', 'App\Http\Controllers\Api\UsersController@update')->name('users.update');
Route::post('users/delete/{id}', 'App\Http\Controllers\Api\UsersController@delete')->name('users.delete');


Route::post('users/login', 'App\Http\Controllers\Api\AuthController@login')->name('users.login');

