<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\User\UserRepositoryInterface; 
use App\Models\User;
use App\Http\Requests\Api\UserFormRequest;
use App\Http\Requests\Api\UserFormUpdateRequest;
use Hash;

class UsersController extends Controller
{
   private $userRepository;

   public function __construct(UserRepositoryInterface $userRepository) {
       // set the model
       $this->userRepository = $userRepository;
   }

   public function index() {
       $page = request()->page;
       $data = $this->userRepository->paginate($page);
       $count = $this->userRepository->countRecords();

       $result['data'] = $data;
       $result['count'] = $count;
    
       return interpretJsonResponse(true, 200, $result);
   }

   public function store(UserFormRequest $request) {
        $data = $this->userRepository->create(request()->all());
    
        return interpretJsonResponse(true, 200, $data);
    }

    public function edit(Request $request) {            
        $data = $this->userRepository->find($request->id);

        return interpretJsonResponse(true, 200, $data);
    }

    public function show(Request $request) {            
        $data = $this->userRepository->find($request->id);

        return interpretJsonResponse(true, 200, $data);
    }

    public function update($id, UserFormUpdateRequest $request) {   
        $dataExist = $this->userRepository->find($id); 

        $formData = $request->all();

        $newData['name'] = $formData['name'];
        $newData['email'] = $formData['email'];

        if(! empty($formData['password'])) {
            $newData['password'] = Hash::make($formData['password']); 
        } else {
            $newData['password'] = $dataExist->password;
        }

        $data = $this->userRepository->updateData($id, $newData);
    
        return interpretJsonResponse(true, 200, $data);
    }

    public function delete($id) {        
        $this->userRepository->delete($id);

        return interpretJsonResponse(true, 204, []);
    }
}
