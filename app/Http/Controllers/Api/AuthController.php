<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\UserLoginFormRequest;
use Hash;
use App\Repositories\User\UserRepositoryInterface;

class AuthController extends Controller
{

    private $userRepository;

   public function __construct(UserRepositoryInterface $userRepository) {
       // set the model
       $this->userRepository = $userRepository;
   }

    public function login(UserLoginFormRequest $request) {
        try {
            $email      = $request->email;
            $password   = $request->password;
            $user       = $this->userRepository->findByEmail($email);

            if (!$user || !Hash::check($request->password, $user->password)) {
                throw new Exception("The provided credentials are incorrect.", 404);
            }    
            
            $token = $user->createToken('my-app-token')->plainTextToken;

            return interpretJsonResponse(true, 200, $token);
        } catch(Exception $e) {
            $statusCode = (! empty($e->getCode())) ? $e->getCode() : 500;
            return interpretJsonErrorResponse("false", $statusCode, $e->getMessage());
        }
    }
}
