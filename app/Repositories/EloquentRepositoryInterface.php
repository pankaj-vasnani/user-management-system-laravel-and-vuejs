<?php

namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface EloquentRepositoryInterface
{
   /**
    * @param array $attributes
    * @return Model
    */
   public function create(array $attributes): Model;

   /**
    * @param array $attributes
    * @return Model
    */
    public function updateData(int $id, array $attributes): ?Model;

    /**
    * @param array $attributes
    * @return Model
    */
    public function delete(int $id): bool;

   /**
    * @param $id
    * @return Model
    */
   public function find($id): ?Model;

   /**
    * @param $email
    * @return Model
    */
    public function findByEmail($email): ?Model;

   /**
    * @param $id
    * @return Model
    */
   public function all(): Collection;

   /**
    * @param $id
    * @return Model
    */
    public function paginate($page): Collection;

    /**
    * @param $id
    * @return Model
    */
    public function countRecords(): Int;
}