<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
// use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (ModelNotFoundException $exception, $request) {
            return interpretJsonResponse(false, $exception->getStatusCode(), [], $exception->getMessage());
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    // public function render($request, Exception $exception)
    // {
    //     $response = $this->handleException($request, $exception);
    //     return $response;
    // }

    // public function handleException($request, Exception $exception)
    // {

    //     if ($exception instanceof MethodNotAllowedHttpException) {
    //         return interpretJsonResponse(false, $exception->getStatusCode(), [], $exception->getMessage());
    //     }

    //     if ($exception instanceof ModelNotFoundException) {
    //         return interpretJsonResponse(false, $exception->getStatusCode(), [], $exception->getMessage());
    //     }

    //     // if ($exception instanceof NotFoundHttpException) {
    //     //     return $this->errorResponse('The specified URL cannot be found', 404);
    //     // }

    //     // if ($exception instanceof HttpException) {
    //     //     return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
    //     // }

    //     if (config('app.debug')) {
    //         return parent::render($request, $exception);            
    //     }
        
    //     return interpretJsonResponse(false, 500, [], 'Unexpected Exception. Try later');

    //     // return $this->errorResponse('Unexpected Exception. Try later', 500);

    // }
}
