import AllUsers from './Components/Users.vue';
import CreateUser from './Components/CreateUser.vue';
import EditUser from './Components/EditUser.vue';
import ShowUser from './Components/ShowUser.vue';
import Login from './Components/Login.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllUsers
    },
    {
        name: 'create',
        path: '/create',
        component: CreateUser
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditUser
    },
    {
        name: 'show',
        path: '/show/:id',
        component: ShowUser
    }
];