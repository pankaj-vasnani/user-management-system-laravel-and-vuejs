<?php
    return [
        "user_type" => [
            "admin" => 1,
            "user" => 2
        ],
        "DATE_FORMAT" => 'd-M-Y H:i:s A'
    ];