<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserDeleteTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test to create the user without email field.
     *
     * @return void
     */
    public function testDeleteUser()
    {
        $user = User::factory()->create();

        $userData = [
            "id" => $user->id
        ];

        $response = $this->json('POST', '/api/users/delete/'.$user->id, $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);
        
        $response->assertStatus(200)
                    ->assertJson([
                        "success" => true,
                        "status" => 204
                    ]);
    }
}
