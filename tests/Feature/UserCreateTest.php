<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Hash;

class UserCreateTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;


    /**
     * Test to get all the users.
     *
     * @return void
     */
    public function testCanGetAllUsers()
    {
        $users = User::factory()->count(3)->make();

        $response = $this->json('GET', '/api/users?page=1', ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertJson([
                        "success" => true,
                        "status" => 200
                    ]);
    }

    /**
     * Test to create the user without name field.
     *
     * @return void
     */
    public function testCreateUserWithoutNameField()
    {
        $userData = [
            "name" => "",
            "email" => $this->faker->unique()->safeEmail,
            "password" => Hash::make("123456"),
            "user_role_id" => 2
        ];

        $response = $this->json('POST', '/api/users/create', $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "success" => false,
                        "status" => 200,
                        "errors" => [
                            "name" => "The name field is required."
                        ]
                    ]);
    }

    /**
     * Test to create the user without email field.
     *
     * @return void
     */
    public function testCreateUserWithoutEmailField()
    {
        $userData = [
            "name" => $this->faker->name,
            "email" => "",
            "password" => Hash::make("123456"),
            "user_role_id" => 2
        ];

        $response = $this->json('POST', '/api/users/create', $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "success" => false,
                        "status" => 200,
                        "errors" => [
                            "email" => "The email field is required."
                        ]
                    ]);
    }

    /**
     * Test to create the user without valid email field.
     *
     * @return void
     */
    public function testCreateUserWithoutValidEmailField()
    {
        $userData = [
            "name" => $this->faker->name,
            "email" => $this->faker->name,
            "password" => Hash::make("123456"),
            "user_role_id" => 2
        ];

        $response = $this->json('POST', '/api/users/create', $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "success" => false,
                        "status" => 200,
                        "errors" => [
                            "email" => "The email must be a valid email address."
                        ]
                    ]);
    }

    /**
     * Test to create the user without password field.
     *
     * @return void
     */
    public function testCreateUserWithoutPasswordField()
    {
        $userData = [
            "name" => $this->faker->name,
            "email" => $this->faker->unique()->safeEmail,
            "password" => "",
            "user_role_id" => 2
        ];

        $response = $this->json('POST', '/api/users/create', $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "success" => false,
                        "status" => 200,
                        "errors" => [
                            "password" => "The password field is required."
                        ]
                    ]);
    }

    /**
     * Test to create the user without valid password field.
     *
     * @return void
     */
    public function testCreateUserWithoutValidPasswordField()
    {
        $userData = [
            "name" => $this->faker->name,
            "email" => $this->faker->unique()->safeEmail,
            "password" => "123",
            "user_role_id" => 2
        ];

        $response = $this->json('POST', '/api/users/create', $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "success" => false,
                        "status" => 200,
                        "errors" => [
                            "password" => "The password must be at least 6 characters."
                        ]
                    ]);
    }

    /**
     * Test to create the user with all valid fields.
     *
     * @return void
     */
    public function testCreateUserWithAllValidFields()
    {
        $userData = [
            "name" => $this->faker->name,
            "email" => $this->faker->unique()->safeEmail,
            "password" => Hash::make("123456"),
            "user_role_id" => 2
        ];

        $response = $this->json('POST', '/api/users/create', $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertJson([
                        "success" => true,
                        "status" => 200
                    ]);
    }
}
