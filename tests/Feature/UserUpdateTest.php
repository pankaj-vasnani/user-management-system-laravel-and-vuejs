<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Hash;

class UserUpdateTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * Test to get the details to edit the user.
     *
     * @return void
     */
    public function testEditUserShowDetails()
    {
        $user = User::factory()->create();

        $userData = [
            "id" => $user->id
        ];

        $response = $this->json('POST', '/api/users/edit', $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "data" => [
                            "created_at" => $user->created_at,
                            "email" => $user->email,
                            "email_verified_at" => $user->email_verified_at,
                            "id" => $user->id,
                            "name" => $user->name,
                            "updated_at" => $user->updated_at,
                            "user_role_id" => "2"
                        ],
                        "message" => "",
                        "success" => true,
                        "status" => 200
                    ]);
    }

    /**
     * Test to get the details to edit the user.
     *
     * @return void
     */
    public function testShowUserDetails()
    {
        $user = User::factory()->create();

        $userData = [
            "id" => $user->id
        ];

        $response = $this->json('POST', '/api/users/show', $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "data" => [
                            "created_at" => $user->created_at,
                            "email" => $user->email,
                            "email_verified_at" => $user->email_verified_at,
                            "id" => $user->id,
                            "name" => $user->name,
                            "updated_at" => $user->updated_at,
                            "user_role_id" => "2"
                        ],
                        "message" => "",
                        "success" => true,
                        "status" => 200
                    ]);
    }

    /**
     * Test to update the user without name field.
     *
     * @return void
     */
    public function testUpdateUserWithoutNameField()
    {
        $user = User::factory()->create();

        $userData = [
            "name" => "",
            "email" => $user->email
        ];

        $response = $this->json('POST', '/api/users/update/'.$user->id, $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "success" => false,
                        "status" => 200,
                        "errors" => [
                            "name" => "The name field is required."
                        ]
                    ]);
    }

    /**
     * Test to create the user without email field.
     *
     * @return void
     */
    public function testUpdateUserWithoutEmailField()
    {
        $user = User::factory()->create();

        $userData = [
            "name" => $user->name,
            "email" => ""
        ];

        $response = $this->json('POST', '/api/users/update/'.$user->id, $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "success" => false,
                        "status" => 200,
                        "errors" => [
                            "email" => "The email field is required."
                        ]
                    ]);
    }

    /**
     * Test to create the user without valid email field.
     *
     * @return void
     */
    public function testUpdateUserWithoutValidEmailField()
    {
        $user = User::factory()->create();

        $userData = [
            "name" => $user->name,
            "email" => $user->name
        ];

        $response = $this->json('POST', '/api/users/update/'.$user->id, $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "success" => false,
                        "status" => 200,
                        "errors" => [
                            "email" => "The email must be a valid email address."
                        ]
                    ]);
    }

    /**
     * Test to create the user without valid password field.
     *
     * @return void
     */
    public function testUpdateUserWithoutValidPasswordField()
    {
        $user = User::factory()->create();

        $userData = [
            "name" => $user->name,
            "email" => $user->email,
            "password" => "123"
        ];

        $response = $this->json('POST', '/api/users/update/'.$user->id, $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertExactJson([
                        "success" => false,
                        "status" => 200,
                        "errors" => [
                            "password" => "The password must be at least 6 characters."
                        ]
                    ]);
    }

    /**
     * Test to create the user with all valid fields.
     *
     * @return void
     */
    public function testUpdateUserWithAllValidFields()
    {
        $user = User::factory()->create();

        $userData = [
            "name" => $user->name,
            "email" => $user->email,
            "password" => Hash::make("123456"),
        ];

        $response = $this->json('POST', '/api/users/update/'.$user->id, $userData, ['Accept' => 'application/json', 'Content-Type' => 'application/json']);

        $response->assertStatus(200)
                    ->assertJson([
                        "success" => true,
                        "status" => 200
                    ]);
    }
}
